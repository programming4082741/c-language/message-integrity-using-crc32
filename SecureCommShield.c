#include <inttypes.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Functions prototypes */
uint32_t rc_crc32(uint32_t crc, const char *hex, size_t len);
void bytes_to_hex_string(const uint8_t *bytes, size_t size, char *hex_string);
void getCurrentDateTime(char *datetime);


/* Variables declaration */
#define MAX_DATA_SIZE 63
size_t data_length;
const char *const_hex_string;
char msgID[19 + 1];
int counter = 1;


/* Define structures */
typedef struct {
    uint32_t mask;
    uint8_t type;
    uint8_t length;
    uint8_t data[MAX_DATA_SIZE];
    uint32_t crc32;
} Message;



int main() {
    /* File declarations*/
    FILE *file_in = fopen("data_in.txt", "r");

    if (file_in == NULL) {
        printf("Unable to open the file.\n");
        return 1;
    }

    FILE *file_out = fopen("data_out.txt", "a");
    if (file_out == NULL) {
        perror("Error opening or creating output file");
        fclose(file_in);
        return 1;
    }

    /* Message declarations */
    Message initMsg;
    Message modMsg;
    getCurrentDateTime(msgID);
    

    /* Reading messages from data_in.txt */
    while (!feof(file_in)) {
        // Read the line from the file
        char line[258];
        if (fgets(line, sizeof(line), file_in) == NULL) {
            // Break the loop if no more lines are available
            break;
        }

        // Check if the line is a mask
        if (strncmp(line, "mask", 4) == 0) {
            // Scan and print in data_out.txt
            sscanf(line, "mask=%8x", &initMsg.mask);
            fprintf(file_out, "Message ID #%s [%d]\n", msgID, counter); counter+=1;
            fprintf(file_out, "Mask:\t\t%08X\n", initMsg.mask);
        }

        // Check if the line is a message
        else if (strncmp(line, "mess", 4) == 0) {
            // Scan initial message type and length
            sscanf(line, "mess=%2x%2x", &initMsg.type, &initMsg.length);
            
            // Scan initial message data
            int dataIndex = 0;
            for (int i = 9; i < 9 + initMsg.length * 2; i += 2) {
                sscanf(line + i, "%2hhx", &initMsg.data[dataIndex++]);
            }

            // Scan initial message crc32
            sscanf(line + 9 + initMsg.length * 2, "%8x", &initMsg.crc32);
            
            // Copy everything from initMsg to modMsg
            memcpy(&modMsg, &initMsg, sizeof(Message));
            

            /* 1. Calculate padding size */
            size_t padding_size = (4 - (modMsg.length % 4)) % 4;

                // Add zero padding if needed
            for (size_t i = 0; i < padding_size; i++) {
                modMsg.data[modMsg.length + i] = 0;
            }
            modMsg.length += padding_size;

                // Ensure modMsg.length does not exceed 63 just in case
            if (modMsg.length > MAX_DATA_SIZE) {
                fprintf(file_out, "Error: Message length exceeds maximum allowed size.\n");
                fclose(file_in);
                fclose(file_out);
                return 1;
            }


            /* 2. Apply mask to even tetrads*/
            for (size_t i = 0; i < modMsg.length; i++) {
                if (i % 2 == 0) {  // Check if it's an even tetrad
                    modMsg.data[i] ^= (modMsg.mask >> 24) & 0xFF;
                    modMsg.data[i] ^= (modMsg.mask >> 16) & 0xFF;
                    modMsg.data[i] ^= (modMsg.mask >> 8) & 0xFF;
                    modMsg.data[i] ^= modMsg.mask & 0xFF;
                }
            }

            // Print initial values
            fprintf(file_out, "Type:\t\t%02X\n", initMsg.type);
            fprintf(file_out, "Initial length:\t%02X\n", initMsg.length);

            fprintf(file_out, "Initial data:\t");
            for (int i = 0; i < initMsg.length; i++) {
                fprintf(file_out, "%02X", initMsg.data[i]);
            }
            fprintf(file_out, "\n");

            fprintf(file_out, "Initial CRC-32:\t%08X\n", initMsg.crc32);

            char *initHexString = malloc(initMsg.length * 2 + 1);
            if (initHexString == NULL) {
                fprintf(file_out, "Memory allocation failed. Cannot check initial CRC-32");
                free(initHexString);
                fclose(file_in);
                fclose(file_out);
                return 1;
            }
            bytes_to_hex_string(initMsg.data, initMsg.length, initHexString);
            const_hex_string = initHexString;
            free(initHexString);
            fprintf(file_out, "Checked CRC-32:\t%08X\n", rc_crc32(0, const_hex_string, strlen(const_hex_string)));


            // Print modified values
            fprintf(file_out, "Modified length:\t\t%02X\n", modMsg.length);
            fprintf(file_out, "Modified data with mask:\t");
            for (int i = 0; i < modMsg.length; i++) {
                fprintf(file_out, "%02X", modMsg.data[i]);
            }
            fprintf(file_out, "\n");

            char *modHexString = malloc(modMsg.length * 2 + 1);
            if (modHexString == NULL) {
                fprintf(file_out, "Memory allocation failed. Cannot check modified CRC-32");
                free(modHexString);
                fclose(file_in);
                fclose(file_out);
                return 1;
            }
            bytes_to_hex_string(modMsg.data, modMsg.length, modHexString);
            const_hex_string = modHexString;
            free(modHexString);
            fprintf(file_out, "Modified CRC-32:\t\t%08X\n", rc_crc32(0, const_hex_string, strlen(const_hex_string)));

            // Print a separator between pairs
            fprintf(file_out, "\n\n");
        }
    }

    // Close the file
    fclose(file_in);
    fclose(file_out);

    return 0;
}


// Custom functions
/*
References:
rc_crc32() -> https://rosettacode.org/wiki/CRC-32#C
*/

uint32_t rc_crc32(uint32_t crc, const char *hex, size_t len) {
    static uint32_t table[256];
    static int have_table = 0;
    uint32_t rem;
    uint8_t octet;
    int i, j;
    const char *p, *q;

    /* This check is not thread safe; there is no mutex. */
    if (have_table == 0) {
        /* Calculate CRC table. */
        for (i = 0; i < 256; i++) {
            rem = i; /* remainder from polynomial division */
            for (j = 0; j < 8; j++) {
                if (rem & 1) {
                    rem >>= 1;
                    rem ^= 0xEDB88320;
                } else
                    rem >>= 1;
            }
            table[i] = rem;
        }
        have_table = 1;
    }

    crc = ~crc;
    q = hex + len;
    for (p = hex; p < q; p += 2) {
        if (*p == '\0' || *(p + 1) == '\0') {
            // Handle incomplete hex character pair
            fprintf(stderr, "Invalid hexadecimal string.\n");
            return 0;  // Or handle error appropriately
        }

        sscanf(p, "%2hhx", &octet);  // Convert hex pair to byte
        crc = (crc >> 8) ^ table[(crc & 0xFF) ^ octet];
    }
    return ~crc;
}


void bytes_to_hex_string(const uint8_t *bytes, size_t size, char *hex_string) {
    for (size_t i = 0; i < size; i++) {
        sprintf(&hex_string[i * 2], "%02X", bytes[i]);
    }
}


void getCurrentDateTime(char *datetime) {
    time_t current_time = time(NULL);
    struct tm *tm_info = localtime(&current_time);
    strftime(datetime, 19 + 1, "%Y-%m-%d %H:%M:%S", tm_info);
}