# SecureComm Shield

SecureComm Shield is a C program designed for secure message processing, incorporating features like padding, masking, and CRC32 checksums to enhance message integrity. This project aims to demonstrate a practical implementation of these cryptographic techniques on message data.

## Features

- **Message Processing:** The program reads messages from the "data_in.txt" file, applies various operations such as masking and padding, and calculates CRC32 checksums to ensure message integrity.

- **File Handling:** Utilizing file operations, the program opens "data_in.txt" for reading and "data_out.txt" for appending. It provides error messages in case of file handling issues.

- **Custom Functions:**
  - `rc_crc32`: Calculates CRC32 checksums for hexadecimal strings using the CRC-32 algorithm.
  - `bytes_to_hex_string`: Converts byte arrays to hexadecimal string representations.
  - `getCurrentDateTime`: Obtains and formats the current date and time.

## Usage

1. **Input Messages:**
   - Messages are expected to be in the "data_in.txt" file, containing information about masks and messages.

2. **Processing:**
   - The program processes each line, identifies masks and messages, applies modifications, and writes results to "data_out.txt."

3. **Output:**
   - Output includes initial and modified values, CRC32 calculations, and a separator between message pairs.

# Acknowledgments

The rc_crc32 function is adapted from Rosetta Code.
